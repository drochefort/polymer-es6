/* global HTMLElement, Polymer */
/**
 * Polymer lifecycle implementation for my-widget custom element
 * see documentation for details: https://www.polymer-project.org/1.0/docs/devguide/registering-elements.html
 */
class MyElement extends HTMLElement {
    /**
     * This method acts like the constructor in Polymer context
     * See: https://www.polymer-project.org/1.0/articles/es6.html
     */
    beforeRegister() {
        console.log('BEFORE REGISTER');

        this.is = 'my-element';

        this.properties = {
            headerTitle: {
                type: String,
                value: 'placeholder title',
                notify: true,
            },
            content: {
                type: String,
                value: 'placeholder content',
                notify: true,
            },
        };
    }

    /**
     * Called when the element has been created,
     * but before property values are set and local DOM is initialized.
     */
    created() {
        console.log('CREATED', this.getAttribute('header-title'));
    }

    /**
     *  Called after property values are set and local DOM is initialized.
     */
    ready() {
        console.log('READY');
    }
}

Polymer(MyElement);
